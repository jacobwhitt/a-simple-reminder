import React from 'react';
import Reminders from './Reminders/Reminders';
import './App.sass';

function App() {
  return (
    <Reminders />
  );
}

export default App;
