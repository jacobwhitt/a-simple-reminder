/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateReminder = /* GraphQL */ `
  subscription OnCreateReminder {
    onCreateReminder {
      id
      index
      title
      priority
      content
    }
  }
`;
export const onUpdateReminder = /* GraphQL */ `
  subscription OnUpdateReminder {
    onUpdateReminder {
      id
      index
      title
      priority
      content
    }
  }
`;
export const onDeleteReminder = /* GraphQL */ `
  subscription OnDeleteReminder {
    onDeleteReminder {
      id
      index
      title
      priority
      content
    }
  }
`;
