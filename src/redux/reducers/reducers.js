import { FETCH_REMINDERS, ADD_REMINDER, REMOVE_REMINDER } from '../actions/actions';

const initialState = {
  reminders: []
};

const rootReducer = (state = initialState, action) => {
  console.log('action in rootReducer', action)
  switch (action.type) {
    case FETCH_REMINDERS:
      return {
        reminders: [...action.reminders],
        index: action.index
      };

    case ADD_REMINDER:
      return {
        reminders: [
          ...state.reminders,
          {
            id: action.id,
            index: action.index,
            title: action.title,
            priority: action.priority,
            content: action.content
          }
        ]
      };

    case REMOVE_REMINDER:
      return {
        reminders: state.reminders.filter((reminder, index) => reminder.index !== action.index)
      };

    default:
      return state;
  };
}

export default rootReducer;