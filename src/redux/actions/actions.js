export const FETCH_REMINDERS = 'FETCH_REMINDERS';
export const ADD_REMINDER = 'ADD_REMINDER';
export const REMOVE_REMINDER = 'REMOVE_REMINDER';

export const fetchReminders = (reminders, index) => {
  return { type: FETCH_REMINDERS, reminders: reminders, index: index, };
}

export const addReminder = (id, index, title, priority, content) => {
  return { type: ADD_REMINDER, id: id, index: index, title: title, priority: priority, content: content };
}

export const removeReminder = (index) => {
  return { type: REMOVE_REMINDER, index: index};
}