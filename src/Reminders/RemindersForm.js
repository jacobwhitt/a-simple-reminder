import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchReminders, addReminder } from '../redux/actions/actions';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { API, graphqlOperation } from 'aws-amplify'
import { createReminder } from '../graphql/mutations'
import { listReminders } from '../graphql/queries'

class RemindersForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      title: '',
      priority: 'low',
      content: '',
      index: 0,
    };
  }

  componentDidMount() {
    this.fetchReminders()
    // console.log('todosLeft in componentDidMount:', todosLeft)
  }

  fetchReminders = async () => {
    try {
      const remindersData = await API.graphql(graphqlOperation(listReminders))
      const reminders = remindersData.data.listReminders.items

      var compare = (a, b) => (a.index > b.index) ? 1 : ((b.index > a.index) ? -1 : 0)
      reminders.sort(compare);

      var index = reminders[reminders.length - 1].index
      console.log('index in fetch:', index)
      this.setState({ index: index + 1 })
      console.log('state index in fetch:', this.state.index)

      this.props.fetchReminders(reminders, index) //? index is dynamically continuous (no duplicate numbers)
    }
    catch (err) {
      console.log('error fetching reminders', err)
    }
    // console.log('this.state in FETCH', this.state)
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
    // console.log('state in handleChange RemindersForm:', this.state)
  }

  handleSubmission = async (e) => {
    e.preventDefault();
    try {
      let { index, title, priority, content } = this.state;

      const response = await API.graphql(graphqlOperation(createReminder, { input: { index: index, title: title, priority: priority, content: content } }))
      var reminder = response.data.createReminder

      this.props.addReminder(reminder.id, reminder.index, reminder.title, reminder.priority, reminder.content);

      this.setState({ index: index + 1, title: '', priority: 'low', content: '' });
    }
    catch (err) {
      console.log('Error creating reminder:', err)
    }
  }

  render() {
    console.log('RemindersForm INDEX:', this.state.index)
    return (
      <React.Fragment>

        <div className='content'>
          <h1>A Simple Reminder</h1>

          <form className='reminderForm column notification is-primary' onSubmit={this.handleSubmission}>


            <div className="field">
              <label className="label" htmlFor='name'>Name</label>
              <div className="control">
                <input className="input  is-small" id='name' type="text" name="title" value={this.state.title} onChange={this.handleChange} placeholder="Give your reminder a title" />
              </div>
            </div>

            <div className="field">
              <label className="label" htmlFor='shortnote'>Short note</label>
              <div className="control">
                <textarea rows="1" className="textarea is-small" id='shortnote' name="content" value={this.state.content} onChange={this.handleChange} placeholder="Type your reminder here"></textarea>
              </div>
            </div>

            <div className='formBtm'>
              <div className="field">
                <label className="label" htmlFor='priority'>Priority</label>
                <div className="control has-text-centered">
                  <div className="select">
                    <select id='priority' name="priority" value={this.state.priority} onChange={this.handleChange}>
                      <option value="low">Low</option>
                      <option value="medium">Medium</option>
                      <option value="high">High</option>
                    </select>
                  </div>
                </div>
              </div>

              <div className="field">
                <div className="control">
                  <button className="button is-link-active" type="submit">Add Reminder</button>
                </div>
              </div>
            </div>

          </form>

        </div>

      </React.Fragment>
    )
  }

}

export default connect(
  null,
  {
    fetchReminders: fetchReminders,
    addReminder: addReminder
  }
)(RemindersForm);