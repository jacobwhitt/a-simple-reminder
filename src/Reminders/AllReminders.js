import React, { Component } from 'react'
import { connect } from 'react-redux';
import { removeReminder } from '../redux/actions/actions';
import { API, graphqlOperation } from 'aws-amplify'
import { deleteReminder } from '../graphql/mutations'
import 'react-bulma-components/dist/react-bulma-components.min.css';



class AllReminders extends Component {


  handleDelete = (props) => {
    console.log('props in handleDelete', props[0], props[1])
    this.removeReminder(props[0])
    this.deleteInDatabase(props[1])
  }

  removeReminder = (idx) => {
    // console.log('redux idx in removeReminder', idx)
    this.props.removeReminder(idx);
  }

  deleteInDatabase = async (id) => {
    var amplifyID = id
    // console.log('amplifyID in deleteInDatabase', amplifyID)
    try {
      await API.graphql(graphqlOperation(deleteReminder, { input: { id: amplifyID } }))
    }
    catch (err) {
      console.log('error deleting reminder', err)
    }

  }



  render() {
    const lowReminders = this.props.reminders.map((reminder, index) => {
      if (reminder.priority === 'low') {
        var propsToPass = [reminder.index, reminder.id, reminder.title]
        // console.log('propsToPass LOW:', propsToPass)

        return <li key={index}>

          <div className="column">
            <div className="reminder notification is-success">
              <article className="media">
                <div className="media-content">
                  <div className="content">
                    <h1 className="title is-size-4">{reminder.title.charAt(0).toUpperCase() + reminder.title.slice(1)}</h1>
                    <p className="is-size-5">{reminder.content.charAt(0).toUpperCase() + reminder.content.slice(1)}</p>
                  </div>
                </div>
                <figure className="media-right">
                  <span className="icon has-text-success">
                    <button className='btn' onClick={() => this.handleDelete(propsToPass)}>X</button>
                  </span>
                </figure>
              </article>
            </div>
          </div>

        </li>
      }
    });

    const mediumReminders = this.props.reminders.map((reminder, index) => {
      if (reminder.priority === 'medium') {
        var propsToPass = [reminder.index, reminder.id, reminder.title]
        // console.log('propsToPass MED:', propsToPass)

        return <li key={index}>

          <div className="column">
            <div className="reminder notification is-warning">
              <article className="media">
                <div className="media-content">
                  <div className="content">
                    <h1 className="title is-size-4">{reminder.title.charAt(0).toUpperCase() + reminder.title.slice(1)}</h1>
                    <p className="is-size-5">{reminder.content.charAt(0).toUpperCase() + reminder.content.slice(1)}</p>
                  </div>
                </div>
                <figure className="media-right">
                  <span className="icon has-text-success">
                    <button className='btn' onClick={() => this.handleDelete(propsToPass)}>X</button>
                  </span>
                </figure>
              </article>
            </div>
          </div>

        </li>
      }
    });

    const highReminders = this.props.reminders.map((reminder, index) => {
      if (reminder.priority === 'high') {
        var propsToPass = [reminder.index, reminder.id, reminder.title]
        // console.log('propsToPass HIGH:', propsToPass)

        return <li key={index}>

          <div className="column">
            <div className="reminder notification is-danger">
              <article className="media">
                <div className="media-content">
                  <div className="content">
                    <h1 className="title is-size-4">{reminder.title.charAt(0).toUpperCase() + reminder.title.slice(1)}</h1>
                    <p className="is-size-5">{reminder.content.charAt(0).toUpperCase() + reminder.content.slice(1)}</p>
                  </div>
                </div>
                <figure className="media-right">
                  <span className="icon has-text-success">
                    <button className='btn' onClick={() => this.handleDelete(propsToPass)}>X</button>
                  </span>
                </figure>
              </article>
            </div>
          </div>

        </li>
      }
    });

    return (
      <React.Fragment>
  
        {
          this.props.reminders.length > 0 ?
            <div className='container'>
              <div className='content'>
                <h3>Your Reminders</h3>
              </div>

              <div className='section'>
                <div className="container">
                  <section className="columns">

                    {/*//* LOW PRIORITY REMINDERS */}
                    <ul className='lowPriority'>
                      <section className="column">
                        <div className="container">
                          <div className="rows">
                            {lowReminders}
                          </div>
                        </div>
                      </section>
                    </ul>

                    {/*//todo MEDIUM PRIORITY REMINDERS */}
                    <ul className='mediumPriority'>
                      <section className="column">
                        <div className="container">
                          <div className="rows">
                            {mediumReminders}
                          </div>
                        </div>
                      </section>
                    </ul>

                    {/*//! HIGH PRIORITY REMINDERS */}
                    <ul className='highPriority'>
                      <section className="column">
                        <div className="container">
                          <div className="rows">
                            {highReminders}
                          </div>
                        </div>
                      </section>
                    </ul>


                  </section>
                </div>
              </div>
            </div>
            : <div className='content'><h3>Reminders will appear here as you add them</h3></div>
        }
      </React.Fragment>
    )
  }

}

const mapStateToProps = state => {
  return {
    reminders: state.reminders
  };
};

const mapDispatchToProprs = {
  removeReminder: removeReminder
};

export default connect(mapStateToProps, mapDispatchToProprs)(AllReminders);