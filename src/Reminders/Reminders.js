import React, { Component } from 'react'
import RemindersForm from './RemindersForm';
import AllReminders from './AllReminders';
import 'react-bulma-components/dist/react-bulma-components.min.css';

export default class Reminders extends Component {
  render() {
    return (
      <React.Fragment>
        <div className='wrapper'>

          <section className="hero">
            <div className="hero-body">
              <div className="container has-text-centered">
                <figure className="center">

                  <RemindersForm />
                  <hr />
                  <AllReminders />

                </figure>
              </div>
            </div>
          </section>
        </div>
      </React.Fragment>
    )
  }
}